import express from "express";
import userRouter from "./userRoute.js";
import restaurantRoute from "./restaurantRoute.js";
const rootRouter = express.Router();

rootRouter.use("/user", userRouter);
rootRouter.use("/restaurant", restaurantRoute);

export default rootRouter;