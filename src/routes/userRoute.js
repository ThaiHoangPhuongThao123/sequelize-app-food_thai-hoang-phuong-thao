import express from "express";
import { getListLikeUser, getListReviewUser, userLikeRestaurant, userOrderFoods, userUnlikeRestaurant } from "../controllers/userController.js";

let userRouter = express.Router();

// user like restaurant
userRouter.post("/user-like-restaurant/:user_id", userLikeRestaurant);

// user unlike restaurant
userRouter.delete("/user-unlike-restaurant/:user_id", userUnlikeRestaurant);

// danh sách like theo user
userRouter.get("/get-list-like-user/:user_id", getListLikeUser);

// danh sách đánh giá theo user
userRouter.get("/get-list-review-user/:user_id", getListReviewUser);

//User đặt món
userRouter.post("/user-order/:user_id", userOrderFoods);

export default userRouter;