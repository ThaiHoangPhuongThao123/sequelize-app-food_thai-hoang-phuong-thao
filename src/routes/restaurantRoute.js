import express from "express";
import { addReview, getListLikeRestaurant, getListReviewsRestaurant } from "../controllers/restaurantController.js";
let restaurantRoute = express.Router();

// danh sách like theo nhà hàng
restaurantRoute.get("/get-list-like-restaurant/:res_id", getListLikeRestaurant);

// danh sách đánh giá theo nhà hàng
restaurantRoute.get("/get-list-review-restaurant/:res_id", getListReviewsRestaurant);

// thêm đánh giá
restaurantRoute.post("/add-restaurant-review/:res_id", addReview);


export default restaurantRoute;