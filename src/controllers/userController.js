import initModels from "../models/init-models.js";
import sequelize from "../models/connect.js";
import moment from "moment";
let model = initModels(sequelize);

 // User like nha hàng
export const userLikeRestaurant = async (req, res) => {
   let date_like = moment.now();
  date_like = moment(date_like).format("YYYY-MM-DD hh:mm:ss");
  let { user_id } = req.params;
   let { res_id } = req.body;
   let listData = await model.like_res.findAll({
    where: {
      user_id: user_id,
      res_id: res_id,
    },
    })
   if (listData.length == 0) {
      await model.like_res.create({user_id, res_id, date_like})
     res.send("Cảm ơn bạn đã like nhà hàng");
   } else {
      res.send("Bạn đã hết lượt like cho nhà hàng này")
  }
}

 // User Unlike nha hàng
export const userUnlikeRestaurant = async (req, res) => {
   let date_like = moment.now();
  date_like = moment(date_like).format("YYYY-MM-DD hh:mm:ss");
    let { user_id } = req.params;
   let {res_id } = req.body;
  await model.like_res.destroy({
    where: {
      user_id: user_id,
      res_id: res_id,
    },
    })
      res.send("Bạn đã unlike cho nhà hàng này")
}

// danh sách like theo user
export const getListLikeUser = async (req, res) => { 
  let { user_id } = req.params;
  let listData = await model.users.findAll({
    where: {
      user_id: user_id,
    },
    include: ["like_res"],
  });
   if (listData.length > 0) {
   res.send(listData);
   } else {
      res.send("Mã người dùng không hợp lệ")
   }
 }

// danh sách đánh giá theo user
export const getListReviewUser = async (req, res) => { 
  await model.rate_res.removeAttribute("id");
  let { user_id } = req.params;
  let listData = await model.users.findAll({
    where: {
      user_id: user_id,
    },
    include: ["rate_res"],
  });
   if (listData.length > 0) {
   res.send(listData);
   } else {
      res.send("Mã người dùng không hợp lệ")
   }

 }

// User đặt món
export const userOrderFoods = async (req, res) => { 
   await model.orders.removeAttribute('id');
    let { user_id } = req.params;
    let { food_id, amount, code, arr_sub_id } = req.body;
    let newData = { user_id, food_id, amount, code, arr_sub_id };
    await model.orders.create(newData);
    res.send("Bạn đã đặt món thành công");
}

 





