import initModels from "../models/init-models.js";
import sequelize from "../models/connect.js";
import moment from "moment";
let model = initModels(sequelize);

// thêm đánh giá
export const addReview = async (req, res) => {
  let { res_id } = req.params;
  let date_rate = moment.now();
  date_rate = moment(date_rate).format("YYYY-MM-DD hh:mm:ss");

  let { user_id, amount } = req.body;
  let data = await model.rate_res.findOne({
    where: {
      res_id: res_id,
      user_id: user_id,
    },
  });
  if (data) {
    res.send("Bạn đã hết lượt đánh giá  cho nhà hàng này ");
  } else {
    let newData = {
      res_id,
      user_id,
      amount,
      date_rate,
    };
    await model.rate_res.create(newData);
    res.send("Cản ơn bạn đã đá giá");
  }
};

// danh sách like theo nhà hàng
export const getListLikeRestaurant = async (req, res) => { 
  let { res_id } = req.params;
  let listData = await model.restaurants.findAll({
    where: {
      res_id: res_id,
    },
    include: ["like_res"],
  });
   if (listData.length > 0) {
   res.send(listData);
   } else {
      res.send("Mã nhà hàng không hợp lệ")
   }
}
 

// danh sách đánh giá theo nhà hàng
export const getListReviewsRestaurant = async (req, res) => {
  await model.rate_res.removeAttribute("id");
  let { res_id } = req.params;
  let listData = await model.restaurants.findAll({
    where: {
      res_id: res_id,
    },
    include: ["like_res"],
  });
  if (listData.length > 0) {
    res.send(listData);
  } else {
    res.send("Mã nhà hàng không đúng")
  }
};



