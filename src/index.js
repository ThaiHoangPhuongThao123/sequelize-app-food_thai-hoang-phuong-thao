import express from "express";
import rootRouter from "./routes/rootRoute.js";
import moment from "moment";
const app = express();
app.use(express.json());
app.listen(8080);
app.use(rootRouter);
