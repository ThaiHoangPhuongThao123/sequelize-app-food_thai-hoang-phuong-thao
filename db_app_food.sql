/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

CREATE TABLE `food` (
  `food_id` int NOT NULL AUTO_INCREMENT,
  `food_name` varchar(500) DEFAULT NULL,
  `image` varchar(1000) DEFAULT NULL,
  `price` float DEFAULT NULL,
  `description` varchar(1000) DEFAULT NULL,
  `type_id` int DEFAULT NULL,
  PRIMARY KEY (`food_id`),
  KEY `type_id` (`type_id`),
  CONSTRAINT `food_ibfk_1` FOREIGN KEY (`type_id`) REFERENCES `food_type` (`type_id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `food_type` (
  `type_id` int NOT NULL AUTO_INCREMENT,
  `type_name` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`type_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `like_res` (
  `user_id` int NOT NULL,
  `res_id` int NOT NULL,
  `date_like` datetime DEFAULT NULL,
  PRIMARY KEY (`user_id`,`res_id`),
  KEY `res_id` (`res_id`),
  CONSTRAINT `like_res_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`),
  CONSTRAINT `like_res_ibfk_2` FOREIGN KEY (`res_id`) REFERENCES `restaurants` (`res_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `orders` (
  `user_id` int DEFAULT NULL,
  `food_id` int DEFAULT NULL,
  `amount` int DEFAULT NULL,
  `code` varchar(1000) DEFAULT NULL,
  `arr_sub_id` varchar(1000) DEFAULT NULL,
  KEY `user_id` (`user_id`),
  KEY `food_id` (`food_id`),
  CONSTRAINT `orders_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`),
  CONSTRAINT `orders_ibfk_2` FOREIGN KEY (`food_id`) REFERENCES `food` (`food_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `rate_res` (
  `user_id` int NOT NULL,
  `res_id` int NOT NULL,
  `amount` int DEFAULT NULL,
  `date_rate` datetime DEFAULT NULL,
  PRIMARY KEY (`user_id`,`res_id`),
  KEY `res_id` (`res_id`),
  CONSTRAINT `rate_res_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`),
  CONSTRAINT `rate_res_ibfk_2` FOREIGN KEY (`res_id`) REFERENCES `restaurants` (`res_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `restaurants` (
  `res_id` int NOT NULL AUTO_INCREMENT,
  `res_name` varchar(500) DEFAULT NULL,
  `image` varchar(1000) DEFAULT NULL,
  `description` varchar(1000) DEFAULT NULL,
  PRIMARY KEY (`res_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `sub_food` (
  `sub_id` int NOT NULL AUTO_INCREMENT,
  `sub_name` varchar(500) DEFAULT NULL,
  `sub_price` float DEFAULT NULL,
  `food_id` int DEFAULT NULL,
  PRIMARY KEY (`sub_id`),
  KEY `food_id` (`food_id`),
  CONSTRAINT `sub_food_ibfk_1` FOREIGN KEY (`food_id`) REFERENCES `food` (`food_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `users` (
  `user_id` int NOT NULL AUTO_INCREMENT,
  `full_name` varchar(300) DEFAULT NULL,
  `email` varchar(400) DEFAULT NULL,
  `password` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

INSERT INTO `food` (`food_id`, `food_name`, `image`, `price`, `description`, `type_id`) VALUES
(1, 'Thịt heo chiên xóc tỏi', 'https://y5kbp0ifnvobj.vcdn.cloud/uploads/filecloud/2018/August/31/1317-895901535701440-1535701440.jpg', 2000, 'Thịt heo chiên xóc tỏi còn biến tấu để trở thành món mồi nhắm cực kỳ tuyệt', 1);
INSERT INTO `food` (`food_id`, `food_name`, `image`, `price`, `description`, `type_id`) VALUES
(2, 'Thịt heo hầm cà rốt', 'https://y5kbp0ifnvobj.vcdn.cloud/uploads/filecloud/2018/August/31/1333-929801535701908-1535701908.jpg', 2500, 'Vị ngọt thanh của cà rốt kết hợp với những miếng thịt lợn đậm đà sẽ là món ăn vô cùng hấp dẫn', 1);
INSERT INTO `food` (`food_id`, `food_name`, `image`, `price`, `description`, `type_id`) VALUES
(3, 'Thịt bò xào hành tây', 'https://cdn.tgdd.vn/2021/04/content/thitboxaohanhtay-800x450.jpg', 3000, 'Thịt bò và hành tây là bộ đôi kết hợp cực ăn ý trong rất nhiều món ăn ngon', 1);
INSERT INTO `food` (`food_id`, `food_name`, `image`, `price`, `description`, `type_id`) VALUES
(4, 'Thịt bò xào lúc lắc', 'https://cdn.tgdd.vn/2021/04/content/thitboxaoluclac-800x450.jpg', 2000, 'Mùi thơm của ớt chuông càng khiến thịt bò thêm dậy mùi', 1),
(5, 'Mực ống chiên nước mắm', 'https://cdn.tgdd.vn/2021/05/content/1-800x450-68.jpg', 3200, 'Mực được chiên nhưng vẫn giữ được độ dai giòn, tươi ngon', 1),
(6, 'Mực chiên muối tắc', 'https://cdn.tgdd.vn/2021/05/content/6-800x450-21.jpg', 2500, 'Mực chiên muối tắc có lớp vỏ vàng giòn', 1),
(7, 'Gà nướng muối ớt', 'https://cdn.tgdd.vn/Files/2020/11/30/1310299/tong-hop-cac-mon-ngon-tu-ga-sieu-ngon-de-nau-tai-nha-202203161150216336.jpg', 2500, ' thịt gà mềm thấm vị mặn của muối, cay nồng của ớt', 1),
(8, 'Tôm hùm nướng bơ tỏi', 'https://naifood.com/wp-content/uploads/2022/11/cac-mon-tu-tom-hum-hap-2.jpg', 5000, 'Kết hợp với tỏi nướng giòn sần sật', 1),
(9, 'Tôm hùm nướng phô mai', 'https://naifood.com/wp-content/uploads/2022/11/cac-mon-tu-tom-hum-nuong-2.jpg', 5300, 'tôm nướng phô mai ghi dấu ấn với vị béo ngậy và thơm lừng của phô mai', 1),
(10, 'Cá hồi sốt cam', 'https://y5kbp0ifnvobj.vcdn.cloud/uploads/filecloud/2020/December/11/7235-955221607652203-1607652203.jpg', 4000, 'Thịt cá hồi mềm thơm quyện cũng vị chua chua, ngọt ngọt, thơm thơm của sốt cam', 1),
(11, 'Coca-Cola', 'https://songseafoodgrill.vn/wp-content/uploads/2022/03/Coca-2-600x560.png', 500, 'Nước ngọt Coca-Cola', 3),
(12, 'Chè trái cây', 'https://imgs.vietnamnet.vn/Images/2017/10/12/15/20171012152957-che-trai-cay-chua-ngot.jpg', 600, 'Chè trái cây phục vụ sau bữa tối', 4),
(13, 'Bánh khoai tây', 'https://i1-giadinh.vnecdn.net/2021/08/27/nh3-1630054035-4780-1630054195.jpg?w=0&h=0&q=100&dpr=2&fit=crop&s=mw8F2sVYgu1wjwc33-aQGw', 600, 'Khoai tây chiên giòn', 2),
(14, 'Canh hấp', 'https://vifon.com.vn/vnt_upload/kitchen/09_2019/thumbs/570_crop_canhrieuthanhmat-Copy.jpg', 800, 'Canh hấp thơm ngon', 2);

INSERT INTO `food_type` (`type_id`, `type_name`) VALUES
(1, 'Món chính');
INSERT INTO `food_type` (`type_id`, `type_name`) VALUES
(2, 'Món Phụ');
INSERT INTO `food_type` (`type_id`, `type_name`) VALUES
(3, 'Nước Uống');
INSERT INTO `food_type` (`type_id`, `type_name`) VALUES
(4, 'Tráng miệng');

INSERT INTO `like_res` (`user_id`, `res_id`, `date_like`) VALUES
(1, 1, '2020-09-05 15:30:10');
INSERT INTO `like_res` (`user_id`, `res_id`, `date_like`) VALUES
(1, 5, '2023-10-17 03:41:30');
INSERT INTO `like_res` (`user_id`, `res_id`, `date_like`) VALUES
(2, 2, '2020-09-01 19:10:19');
INSERT INTO `like_res` (`user_id`, `res_id`, `date_like`) VALUES
(2, 5, '2020-05-09 10:42:11'),
(3, 1, '2020-10-09 11:16:10'),
(3, 3, '2020-02-02 12:12:12'),
(4, 1, '2020-02-13 23:40:20'),
(5, 1, '2020-01-01 10:10:10'),
(5, 6, '2020-07-07 10:10:17'),
(6, 1, '2020-07-08 09:10:06'),
(6, 4, '2020-01-01 10:10:10'),
(7, 2, '2020-06-06 18:16:16');

INSERT INTO `orders` (`user_id`, `food_id`, `amount`, `code`, `arr_sub_id`) VALUES
(1, 14, 2, 'ORD123', '1-2');
INSERT INTO `orders` (`user_id`, `food_id`, `amount`, `code`, `arr_sub_id`) VALUES
(1, 5, 2, 'ORD124', '3');
INSERT INTO `orders` (`user_id`, `food_id`, `amount`, `code`, `arr_sub_id`) VALUES
(3, 5, 2, 'ORD125', '1-2');
INSERT INTO `orders` (`user_id`, `food_id`, `amount`, `code`, `arr_sub_id`) VALUES
(4, 4, 2, 'ORD126', '1-3'),
(7, 14, 2, 'ORD127', '3'),
(5, 6, 2, 'ORD128', '2-3'),
(3, 1, 3, 'ORD188', '2-3'),
(3, 1, 3, 'ORD188', '2-3'),
(3, 1, 3, 'ORD188', '2-3'),
(3, 1, 3, 'ORD188', '2-3'),
(4, 3, 2, 'ORD1908', '2-1');

INSERT INTO `rate_res` (`user_id`, `res_id`, `amount`, `date_rate`) VALUES
(1, 1, 3, '2020-09-05 15:33:10');
INSERT INTO `rate_res` (`user_id`, `res_id`, `amount`, `date_rate`) VALUES
(2, 2, 4, '2020-09-01 19:15:19');
INSERT INTO `rate_res` (`user_id`, `res_id`, `amount`, `date_rate`) VALUES
(2, 3, 3, '2023-10-07 18:06:22');
INSERT INTO `rate_res` (`user_id`, `res_id`, `amount`, `date_rate`) VALUES
(2, 5, 3, '2020-05-09 10:45:11'),
(2, 6, 3, '2023-10-17 04:01:07'),
(3, 1, 4, '2020-10-09 11:13:10'),
(3, 3, 4, '2020-02-02 12:14:12'),
(3, 6, 3, '2023-10-17 04:02:32'),
(4, 1, 4, '2020-02-13 23:41:20'),
(5, 1, 4, '2020-01-01 10:11:10'),
(5, 6, 5, '2020-07-07 10:18:17'),
(6, 1, 5, '2020-07-08 09:19:06'),
(6, 4, 3, '2020-01-01 10:15:10'),
(7, 2, 4, '2020-06-06 18:26:16');

INSERT INTO `restaurants` (`res_id`, `res_name`, `image`, `description`) VALUES
(1, 'Hệ thống Nhà hàng Phố 79', 'https://cdn.justfly.vn/1920x1281/media/202104/26/1619433770-he-thong-nha-hang-pho-79-sai-gon.jpg', 'Kết hợp giữa kiến trúc hiện đại của phương Tây và nét hoài cổ Việt');
INSERT INTO `restaurants` (`res_id`, `res_name`, `image`, `description`) VALUES
(2, 'Nhà hàng Maison Mận-Đỏ', 'https://cdn.justfly.vn/1920x1283/media/202302/27/1677500379-khong-gian-nha-hang-maison-mando.jpg', 'Với lối kiến trúc Đông Dương độc đáo, sang trọng giao thoa giữa lối kiến trúc Châu Âu');
INSERT INTO `restaurants` (`res_id`, `res_name`, `image`, `description`) VALUES
(3, 'Chuỗi Ân Nam Quán', 'https://cdn.justfly.vn/1920x1081/media/07/19/3a0e-392d-478a-b583-67c382a011a5.jpg', 'Thực đơn của chuỗi Ân Nam Quán có tới hơn 80 món ăn đặc sản nổi tiếng của miền Trung');
INSERT INTO `restaurants` (`res_id`, `res_name`, `image`, `description`) VALUES
(4, 'Nhà hàng Sườn Nướng Hàn Quốc', 'https://cdn.justfly.vn/1920x1280/media/30/f7/d082-5653-4666-8b2f-70ad2e7b229c.jpg', 'Sườn Nướng Hàn Quốc trở thành quán ăn ngon thu hút khá đông giới trẻ Sài Gòn'),
(5, ' Chuỗi Lẩu Cua Đất Mũi', 'https://cdn.justfly.vn/1920x1280/media/39/68/a5b7-83f3-402a-9062-a1853d5d13af.jpg', ' Hầu hết các thực khách đến với nhà hàng này là nhờ thực đơn vô cùng hấp dẫn'),
(6, 'Chuỗi Wrap & Roll', 'https://cdn.justfly.vn/1920x1440/media/e5/23/29e8-d3a1-4d35-aaa8-bc3ec5864260.jpg', 'Wrap & Roll đều được tọa lạc ở những vị trí sang trọng và lịch sự'),
(7, 'Chuỗi San Fu Lou', 'https://cdn.justfly.vn/1920x1245/media/b6/15/d2f4-38b6-403b-b477-fc6b1cb4c5f4.jpg', 'San Fu Lou là chuỗi nhà hàng mang phong cách cổ điển Trung Hoa cao cấp'),
(8, 'Chuỗi Dìn Ký', 'https://cdn.justfly.vn/1920x1281/media/fa/0b/301f-f251-4524-90fd-61d1b46cc29b.jpg', 'Gần 400 món ăn đặc sắc với đa dạng phong cách ẩm thực');

INSERT INTO `sub_food` (`sub_id`, `sub_name`, `sub_price`, `food_id`) VALUES
(1, 'Bánh khoai tây', 600, 13);
INSERT INTO `sub_food` (`sub_id`, `sub_name`, `sub_price`, `food_id`) VALUES
(2, 'Canh cà chua', 800, 14);
INSERT INTO `sub_food` (`sub_id`, `sub_name`, `sub_price`, `food_id`) VALUES
(3, 'Coca-Cola', 500, 11);

INSERT INTO `users` (`user_id`, `full_name`, `email`, `password`) VALUES
(1, 'Nguyễn Văn A', 'a@ggmail.com', '1234a');
INSERT INTO `users` (`user_id`, `full_name`, `email`, `password`) VALUES
(2, 'Nguyễn Văn Bảnh', 'banh@ggmail.com', '1234banh');
INSERT INTO `users` (`user_id`, `full_name`, `email`, `password`) VALUES
(3, 'Nguyễn Văn Huệ', 'hue@ggmail.com', '1234hue');
INSERT INTO `users` (`user_id`, `full_name`, `email`, `password`) VALUES
(4, 'Lê Ngọc Hoa', 'ngochoa@ggmail.com', '1234hoa'),
(5, 'Nguyễn Ngọc Mai', 'mai@ggmail.com', '1234mai'),
(6, 'Trần Tố Uyên', 'uyen@ggmail.com', '123uyen'),
(7, 'Phạm Hà Thanh', 'thanh@ggmail.com', 'thanhthanh'),
(8, 'Phạm Văn Hùng', 'hung@ggmail.com', 'hung44');


/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;